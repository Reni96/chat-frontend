import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sub-navbar',
  templateUrl: './sub-navbar.component.html',
  styleUrls: ['./sub-navbar.component.scss'],
})
export class SubNavbarComponent {
  @Input() title: string;
  @Input() link: string;
  @Input() back = undefined;
}
