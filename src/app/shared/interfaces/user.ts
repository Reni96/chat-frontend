export interface User {
  id?: number;
  name?: string;
  email: string;
  salt?: string;
  passwordHash?: string;
}
