export interface Room {
  id: number;
  name: string;
  password: string;
}
