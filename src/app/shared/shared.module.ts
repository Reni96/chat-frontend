import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardComponent } from '@shared/components/card/card.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SubNavbarComponent } from './components/sub-navbar/sub-navbar.component';

const IMPORTS = [FormsModule, HttpClientModule, ReactiveFormsModule];
const DECLARATIONS = [CardComponent, NavbarComponent, SubNavbarComponent];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ...IMPORTS,
   ],
  declarations: [...DECLARATIONS],
  exports: [...DECLARATIONS, ...IMPORTS],
})
export class SharedModule {}
