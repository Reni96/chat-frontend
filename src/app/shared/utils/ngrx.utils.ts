import { createAction } from '@ngrx/store';
import { Props, NotAllowedCheck } from '@ngrx/store/src/models';

type StrictObject = Record<string, unknown>;
const SIMPLE_TYPE_REF = createAction('noop' as string);
export function createApiAction<
  TFetch extends StrictObject | undefined = undefined,
  TSuccess extends StrictObject | undefined = undefined,
  TFail extends StrictObject | undefined = undefined
>(
  name: string,
  props: {
    fetch?: Props<TFetch> & NotAllowedCheck<TFetch>;
    success?: Props<TSuccess> & NotAllowedCheck<TSuccess>;
    fail?: Props<TFail> & NotAllowedCheck<TFail>;
  }
) {
  const fetch = createAction(`${name}Fetch`, props.fetch);
  const success = createAction(`${name}Success`, props.success);
  const fail = createAction(`${name}Fail`, props.fail);
  return {
    fetch: fetch as TFetch extends undefined ? typeof SIMPLE_TYPE_REF : typeof fetch,
    success: success as TSuccess extends undefined ? typeof SIMPLE_TYPE_REF : typeof success,
    fail: fail as TFail extends undefined ? typeof SIMPLE_TYPE_REF : typeof fail
  };
}
