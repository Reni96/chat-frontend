import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateRoomComponent } from './pages/create-room/create-room.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { RoomDetailComponent } from './pages/room-detail/room-detail.component';
import { RoomsComponent } from './pages/rooms/rooms.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  {
    path: '',
    children: [
      {
        path: 'login',
        loadChildren: () => import('./pages/login-module/login.module').then(m => m.LoginModule)
      },
    ]
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
  },
  {
    path: 'signup',
    component: RegistrationComponent,
  },
  {
    path: 'rooms',
    component: RoomsComponent,
  },
  { path: 'rooms/create', component: CreateRoomComponent },
  { path: 'rooms/:id/detail', component: RoomDetailComponent },
  { path: 'rooms/:id/messages', component: MessagesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
