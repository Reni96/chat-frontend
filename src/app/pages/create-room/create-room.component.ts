import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '@app/service/service.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss'],
})
export class CreateRoomComponent {
  form = this.fb.group({
    name: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private service: ServiceService, private router: Router) {}

  save() {
    if (this.form.dirty && this.form.valid) {
      this.service
        .createRoom(this.form.value)
        .pipe(take(1))
        .subscribe(() => this.router.navigate(['/rooms']));
    }
  }
}
