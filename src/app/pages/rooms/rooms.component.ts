import { Component } from '@angular/core';
import { ServiceService } from '@app/service/service.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss'],
})
export class RoomsComponent {
  rooms$ = this.service.getRooms();
  constructor(private service: ServiceService) {}
}
