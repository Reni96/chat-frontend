import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '@app/service/service.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  form = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    name: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private service: ServiceService, private router: Router) {}

  save() {
    if (this.form.dirty && this.form.valid) {
      this.service
        .register(this.form.value)
        .pipe(take(1))
        .subscribe(() => this.router.navigate(['/login']));
    }
  }
}
