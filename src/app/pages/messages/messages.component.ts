import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '@app/service/service.service';
import { User } from '@shared/interfaces';
import { Socket } from 'ngx-socket-io';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {
  id = this.route.snapshot.params.id;
  socketMessages: string[] = [];
  messages$ = this.service.getMessages(this.route.snapshot.params.id);
  getUser$ = this.service.getUser();
  getUsers$ =  this.service.getUsers();

  chatMessages$ = this.socket.fromEvent(`chat-${this.id}`).pipe(
    map((message: string) => {
      this.socketMessages = [...this.socketMessages, message];
      return this.socketMessages;
    }),
  );

  form = this.fb.group({
    message: ['', Validators.required],
  });

  constructor(
    private service: ServiceService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private socket: Socket,
  ) {}

  ngOnInit() {
    console.log('id:', this.id);
  }

  send(form: any, user: User) {
    if (this.form.dirty && this.form.valid) {
      this.service
        .sendMessage({
          roomId: this.id as number,
          createdAt: new Date().toLocaleString(),
          data: form.value['message'],
          userId: user.id,
        })
        .pipe(take(1))
        .subscribe();
    }
    this.form.reset();
    console.log(form.value['message']);
  }
}
