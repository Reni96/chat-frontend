import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getUser, login, logout, register } from './actions';
import { loginQuery } from './selectors';


@Injectable({
  providedIn: 'any',
})
export class LoginFacade {
  isLoggedIn$ = this.store.pipe(select(loginQuery.isLoggedIn));
  user$ = this.store.pipe(select(loginQuery.user));

  constructor(private store: Store) {}

  login(user: { email: string; password: string }) {
    this.store.dispatch(login.fetch(user));
  }

  logout() {
    this.store.dispatch(logout.fetch());
  }

  register(user: { email: string; password: string }) {
    this.store.dispatch(register.fetch(user));
  }

  getUser() {
    this.store.dispatch(getUser.fetch());
  }
}
