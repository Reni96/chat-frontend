import { props } from '@ngrx/store';
import { createApiAction } from '../../../../app/shared/utils/ngrx.utils';

export const login = createApiAction('[Login Actions] login', {
  fetch: props<{ email: string, password: string }>(),
  fail: props<{ error: string }>()
});

export const logout = createApiAction('[Login Actions] logout', {
  fail: props<{ error: string }>()
});

export const register = createApiAction('[Login Actions] register', {
  fetch: props<{ email: string, password: string }>(),
  success: props<{ email: string, id: number }>(),
  fail: props<{ error: string }>()
});

export const getUser = createApiAction('[Login Actions] getUser', {
  success: props<{ user: {email: string, id: number} }>(),
  fail: props<{ error: string }>()
});
