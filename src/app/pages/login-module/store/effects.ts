import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ServiceService } from '../../../service/service.service';
import { getUser, login, register } from './actions';

@Injectable()
export class LoginEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login.fetch),
      switchMap(({ email, password }) => {
        return this.apiService.login({ email, password }).pipe(
          map(() => {

            this.router.navigate(['rooms']);
            return login.success();
          }),
          catchError(err => {
            return of(login.fail({ error: err.message }));
          }),
        );
      }),
    ),
  );

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login.success),
      map(() => {
        this.router.navigate(['rooms']);
        return getUser.fetch();
      }),
    ),
  );

  getUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUser.fetch),
      switchMap(() => {
        return this.apiService.getUser().pipe(
          map((user: { email: string; id: number }) => {
            return getUser.success({ user });
          }),
          catchError(err => {
            return of(getUser.fail({ error: err.message }));
          }),
        );
      }),
    ),
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(register.fetch),
      switchMap((user: { email: string; password: string }) => {
        return this.apiService.register(user).pipe(
          map((user: { email: string; id: number }) => {
            return register.success(user);
          }),
          catchError(err => {
            return of(register.fail({ error: err.message }));
          }),
        );
      }),
    ),
  );
  constructor(private actions$: Actions, private apiService: ServiceService, private router: Router) {}
}
