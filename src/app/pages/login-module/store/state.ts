import { User } from "../../../../app/shared/interfaces/user";

export const initialLoginState: LoginState = {
  isLoggedIn: false,
  user: {email: '', id: null},
  errorMessage: '',
};

export interface LoginState {
  isLoggedIn: boolean;
  user: User;
  errorMessage: string;
}
