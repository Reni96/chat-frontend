import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LoginState } from './state';

const featureSelector = createFeatureSelector<LoginState>('login');

const isLoggedIn = createSelector(featureSelector, state => state.isLoggedIn);
const user = createSelector(featureSelector, state => state.user)

export const loginQuery = {
  isLoggedIn,
  user
};
