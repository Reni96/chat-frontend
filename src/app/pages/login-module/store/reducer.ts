import { Action, createReducer, on } from '@ngrx/store';
import { getUser, login, logout } from './actions';
import { LoginState, initialLoginState } from './state';

const reducer = createReducer<LoginState>(
  initialLoginState,
  on(login.success, (state) => ({
    ...state,
    isLoggedIn: true,
  })),
  on(logout.success, (state) => ({
    ...state,
    isLoggedIn: false,
  })),
  on(getUser.success, (state, { user }) => ({
    ...state,
    user: {email: user.email, id: user.id}
  })),
);

export function loginReducer(state: LoginState, action: Action) {
  return reducer(state, action);
}
