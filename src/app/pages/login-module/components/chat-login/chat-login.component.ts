import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginFacade } from '../../store/facade';

@Component({
  selector: 'app-chat-login',
  templateUrl: './chat-login.component.html',
  styleUrls: ['./chat-login.component.scss'],
  // providers: [
  //   LoginFacade // added class in the providers
  // ]
})
export class ChatLoginComponent {
  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private loginFacade: LoginFacade, private router: Router) {}

  login(form: FormGroup) {
    console.log('form:', form);
    if (form.valid) {
      this.loginFacade.login(form.value);
    }
  }

  register(form: FormGroup) {
    if (form.valid) {
      this.loginFacade.register(form.value);
      this.loginForm.setValue({
        email: '',
        password: '',
      });
    }
  }
}
