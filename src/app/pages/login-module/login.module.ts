import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ChatLoginComponent } from './components/chat-login/chat-login.component';
import { LoginFacade } from './store/facade';
import { LoginEffects } from './store/effects';
import { loginReducer } from './store/reducer';

@NgModule({
  declarations: [
    ChatLoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    StoreModule.forFeature('login', loginReducer),
    EffectsModule.forFeature([LoginEffects])
  ],
  providers: [LoginEffects, LoginFacade],

})
export class LoginModule { }
