import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '@app/service/service.service';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.scss'],
})
export class RoomDetailComponent {
  room$ = this.service.getRoom(this.route.snapshot.params.id);
  constructor(private service: ServiceService, private route: ActivatedRoute) {}
}
