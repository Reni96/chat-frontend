import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateRoomComponent } from './pages/create-room/create-room.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { RoomDetailComponent } from './pages/room-detail/room-detail.component';
import { RoomsComponent } from './pages/rooms/rooms.component';
import { ServiceService } from './service/service.service';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    RoomsComponent,
    LandingPageComponent,
    CreateRoomComponent,
    RoomDetailComponent,
    WelcomeComponent,
    RegistrationComponent,
    MessagesComponent,
  ],
  imports: [
    SharedModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    SocketIoModule.forRoot(config),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    }),
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
