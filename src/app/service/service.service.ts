import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, Room, User } from '@shared/interfaces';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';

const BASE_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  constructor(private http: HttpClient) {}

  public getRooms() {
    return this.http.get<Room>(`${BASE_URL}/api/rooms`);
  }

  public createRoom(roomDetails: Room) {
    return this.http.post<Room>(`${BASE_URL}/api/rooms`, roomDetails);
  }

  public getRoom(id: number) {
    return this.http.get<Room>(`${BASE_URL}/api/rooms/${id}`);
  }

  public login(loginData: { email: string; password: string }) {
    return this.http.post<User>(`${BASE_URL}/api/login`, loginData).pipe(tap(console.log));
  }

  public register(registerData: User) {
    return this.http.post<User>(`${BASE_URL}/api/signup`, registerData);
  }

  public getUser() {
    return this.http.get<User>(`${BASE_URL}/api/me`);
  }

  public getUserById() {
    return this.http.get<User>(`${BASE_URL}/api/user`);
  }

  public getUsers() {
    return this.http.get<User>(`${BASE_URL}/api/users`);
  }

  public sendMessage(message: Message) {
    return this.http.post<Message>(`${BASE_URL}/api/rooms/${message.roomId}/messages`, { message });
  }

  public getMessages(id: number) {
    return this.http.get<Message>(`${BASE_URL}/api/rooms/${id}/messages`);
  }
}
